# This is free and unencumbered software released into the public domain.

library(reshape)

d <- read.csv('delete-specific.csv', header = FALSE, col.names = c("test.name", "container", "n.iterations", "n.elements", "time"), strip.white = TRUE)
test.name <- d$test.name[1]
n.iterations <- d$n.iterations[1]
d$test.name <- NULL
d$n.iterations <- NULL

d2 <- cast(d, n.elements ~ container)

# Whole plot
pdf('delete-specific.pdf')
plot(d2$n.elements, d2$GList, 'b', main='Time to delete a specific element from an N-element container', xlab='N elements', ylab=expression(paste('Time (', mu, 's)')), col='green', log='x')
lines(d2$n.elements, d2$GSList, 'b', col='red', pch=3)
lines(d2$n.elements, d2$GPtrArray, 'b', col='blue', pch=4)
lines(d2$n.elements, d2$GArray, 'b', col='yellow', pch=2)
dev.off()

# Zoom in on the origin
pdf('delete-specific-zoom.pdf')
plot(d2$n.elements, d2$GList, 'b', main='Time to delete a specific element from an N-element container (zoomed)', xlab='N elements', ylab=expression(paste('Time (', mu, 's)')), col='green', log='x', xlim=c(10000, 200000), ylim=c(0, 500000))
lines(d2$n.elements, d2$GSList, 'b', col='red', pch=3)
lines(d2$n.elements, d2$GPtrArray, 'b', col='blue', pch=4)
lines(d2$n.elements, d2$GArray, 'b', col='yellow', pch=2)
dev.off()
