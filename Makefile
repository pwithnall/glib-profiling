containers: containers.c
	gcc -o $@ $< -O3 -Wall `pkg-config --cflags --libs glib-2.0`

%.csv: containers
	./containers $(basename $@) > $@
%.pdf: %.R %.csv
	R --vanilla < $^

tests = \
	clear \
	create \
	delete-inspecific \
	delete-specific \
	insert-inspecific \
	insert-specific \
	iterate \
	$(NULL)

all: $(addsuffix .pdf,$(tests)) $(addsuffix .csv,$(tests))
clean:
	rm -f $(addsuffix .pdf,$(tests))
	rm -f $(addsuffix -zoom.pdf,$(tests))
	rm -f $(addsuffix .csv,$(tests))
	rm -f containers

.PHONY: all clean
