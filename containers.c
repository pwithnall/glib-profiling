/* This is free and unencumbered software released into the public domain. */

#include <string.h>

#include <glib.h>


typedef struct {
	void (*destroy) (gpointer collection);

	/* Create a collection with n_elements elements, in no specific
	 * order. */
	gpointer (*create) (guint n_elements);

	/* Iterate over every element in the collection, in no specific order.
	 * Call iteration_func() on each element. */
	void (*iterate) (gpointer collection);

	/* Insert a single element at a specific location in an existing
	 * collection. */
	gpointer (*insert_specific) (gpointer collection, guint idx);

	/* Insert a single element at an arbitrary location in an existing
	 * collection (i.e. at whichever location is fastest to insert at). */
	gpointer (*insert_inspecific) (gpointer collection);

	/* Delete a single element from a specific location in an existing
	 * collection. */
	gpointer (*delete_specific) (gpointer collection, guint idx);

	/* Delete a single element from an arbitrary location in an existing
	 * collection (i.e. whichever element is fastest to delete). */
	gpointer (*delete_inspecific) (gpointer collection);

	/* Delete every element from an existing collection. */
	gpointer (*clear) (gpointer collection);
} CollectionFuncs;

typedef struct {
	/* Number of elements to initially create in the collection. */
	guint n_elements;

	/* Number of iterations of the test to perform to increase
	 * significance. */
	guint n_iterations;
} TestData;


/* A dummy function which theoretically performs some work during a single
 * iteration over a container. */
static void
iteration_func (gconstpointer data)
{
	/* Do nothing. */
}


/*
 * Collection functions.
 */
static gpointer
collection_glist_create (guint n_elements)
{
	guint i;
	GList *collection = NULL;
	gpointer data = NULL;  /* dummy element data */

	for (i = 0; i < n_elements; i++) {
		collection = g_list_prepend (collection, data);
	}

	return collection;
}

static void
collection_glist_destroy (gpointer col)
{
	GList *collection = col;

	g_list_free (collection);
}

static void
collection_glist_iterate (gpointer col)
{
	GList *collection = col, *l;

	for (l = collection; l != NULL; l = l->next) {
		iteration_func (l->data);
	}
}

static gpointer
collection_glist_insert_specific (gpointer col, guint idx)
{
	GList *collection = col;
	gpointer data = NULL;  /* dummy element data */

	return g_list_insert (collection, data, idx);
}

static gpointer
collection_glist_insert_inspecific (gpointer col)
{
	GList *collection = col;
	gpointer data = NULL;  /* dummy element data */

	return g_list_prepend (collection, data);
}

static gpointer
collection_glist_delete_specific (gpointer col, guint idx)
{
	GList *collection = col;
	GList *i = g_list_nth (collection, idx);

	return g_list_delete_link (collection, i);
}

static gpointer
collection_glist_delete_inspecific (gpointer col)
{
	GList *collection = col;

	return g_list_delete_link (collection, collection);
}

static gpointer
collection_glist_clear (gpointer col)
{
	GList *collection = col;

	g_list_free (collection);

	return NULL;
}

static gpointer
collection_gslist_create (guint n_elements)
{
	guint i;
	GSList *collection = NULL;
	gpointer data = NULL;  /* dummy element data */

	for (i = 0; i < n_elements; i++) {
		collection = g_slist_prepend (collection, data);
	}

	return collection;
}

static void
collection_gslist_destroy (gpointer col)
{
	GSList *collection = col;

	g_slist_free (collection);
}

static void
collection_gslist_iterate (gpointer col)
{
	GSList *collection = col, *l;

	for (l = collection; l != NULL; l = l->next) {
		iteration_func (l->data);
	}
}

static gpointer
collection_gslist_insert_specific (gpointer col, guint idx)
{
	GSList *collection = col;
	gpointer data = NULL;  /* dummy element data */

	return g_slist_insert (collection, data, idx);
}

static gpointer
collection_gslist_insert_inspecific (gpointer col)
{
	GSList *collection = col;
	gpointer data = NULL;  /* dummy element data */

	return g_slist_prepend (collection, data);
}

static gpointer
collection_gslist_delete_specific (gpointer col, guint idx)
{
	GSList *collection = col;
	GSList *i = g_slist_nth (collection, idx);

	return g_slist_delete_link (collection, i);
}

static gpointer
collection_gslist_delete_inspecific (gpointer col)
{
	GSList *collection = col;

	return g_slist_delete_link (collection, collection);
}

static gpointer
collection_gslist_clear (gpointer col)
{
	GSList *collection = col;

	g_slist_free (collection);

	return NULL;
}

static gpointer
collection_gptrarray_create (guint n_elements)
{
	guint i;
	GPtrArray *collection;
	gpointer data = NULL;  /* dummy element data */

	collection = g_ptr_array_new ();
	for (i = 0; i < n_elements; i++) {
		g_ptr_array_add (collection, data);
	}

	return collection;
}

static void
collection_gptrarray_destroy (gpointer col)
{
	GPtrArray *collection = col;

	g_ptr_array_unref (collection);
}

static void
collection_gptrarray_iterate (gpointer col)
{
	GPtrArray *collection = col;
	guint k;

	for (k = 0; k < collection->len; k++) {
		iteration_func (g_ptr_array_index (collection, k));
	}
}

static gpointer
collection_gptrarray_insert_specific (gpointer col, guint idx)
{
	GPtrArray *collection = col;
	guint i;
	gpointer data = NULL;  /* dummy element data */

	g_ptr_array_add (collection, NULL);
	/* Shift every other element down one place. */
	for (i = idx; i < collection->len - 1; i++) {
		g_ptr_array_index (collection, i + 1) =
			g_ptr_array_index (collection, i);
	}

	g_ptr_array_index (collection, idx) = data;

	return collection;
}

static gpointer
collection_gptrarray_insert_inspecific (gpointer col)
{
	GPtrArray *collection = col;
	gpointer data = NULL;  /* dummy element data */

	g_ptr_array_add (collection, data);

	return collection;
}

static gpointer
collection_gptrarray_delete_specific (gpointer col, guint idx)
{
	GPtrArray *collection = col;

	g_ptr_array_remove_index_fast (collection, idx);

	return collection;
}

static gpointer
collection_gptrarray_delete_inspecific (gpointer col)
{
	GPtrArray *collection = col;

	g_ptr_array_remove_index_fast (collection, collection->len - 1);

	return collection;
}

static gpointer
collection_gptrarray_clear (gpointer col)
{
	GPtrArray *collection = col;

	g_ptr_array_set_size (collection, 0);

	return collection;
}

static gpointer
collection_garray_create (guint n_elements)
{
	guint i;
	GArray *collection;
	gpointer data = NULL;  /* dummy element data */

	collection = g_array_sized_new (FALSE, FALSE, sizeof (gpointer),
	                                n_elements);
	for (i = 0; i < n_elements; i++) {
		g_array_append_val (collection, data);
	}

	return collection;
}

static void
collection_garray_destroy (gpointer col)
{
	GArray *collection = col;

	g_array_unref (collection);
}

static void
collection_garray_iterate (gpointer col)
{
	GArray *collection = col;
	guint k;

	for (k = 0; k < collection->len; k++) {
		iteration_func (g_array_index (collection, gpointer, k));
	}
}

static gpointer
collection_garray_insert_specific (gpointer col, guint idx)
{
	GArray *collection = col;
	gpointer data = NULL;  /* dummy element data */

	g_array_insert_val (collection, idx, data);

	return collection;
}

static gpointer
collection_garray_insert_inspecific (gpointer col)
{
	GArray *collection = col;
	gpointer data = NULL;  /* dummy element data */

	g_array_append_val (collection, data);

	return collection;
}

static gpointer
collection_garray_delete_specific (gpointer col, guint idx)
{
	GArray *collection = col;

	g_array_remove_index_fast (collection, idx);

	return collection;
}

static gpointer
collection_garray_delete_inspecific (gpointer col)
{
	GArray *collection = col;

	g_array_remove_index_fast (collection, collection->len - 1);

	return collection;
}

static gpointer
collection_garray_clear (gpointer col)
{
	GArray *collection = col;

	g_array_set_size (collection, 0);

	return collection;
}


/*
 * Test functions. Each test returns a string with its results. The string must
 * be freed using g_free().
 */
static gchar *
test_create (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time, aggregate_time = 0;

	/* Run the test. */
	for (j = 0; j < data->n_iterations; j++) {
		start_time = g_get_monotonic_time ();
		collection = funcs->create (data->n_elements);
		end_time = g_get_monotonic_time ();

		/* Tidy up. */
		funcs->destroy (collection);

		aggregate_time += (end_time - start_time);
	}

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        (aggregate_time * 1000) / data->n_iterations);
}

static gchar *
test_iterate (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time;

	/* Create a large collection. */
	collection = funcs->create (data->n_elements);

	/* Run the test. */
	start_time = g_get_monotonic_time ();

	for (j = 0; j < data->n_iterations; j++) {
		funcs->iterate (collection);
	}

	end_time = g_get_monotonic_time ();

	/* Tidy up. */
	funcs->destroy (collection);

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        ((end_time - start_time) * 1000) /
	                        data->n_iterations);
}

static gchar *
test_insert_specific (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time;

	/* Create a collection. */
	collection = funcs->create (data->n_elements);

	/* Run the test. */
	start_time = g_get_monotonic_time ();

	for (j = 0; j < data->n_iterations; j++) {
		collection = funcs->insert_specific (collection,
		                                     data->n_elements / 2);
	}

	end_time = g_get_monotonic_time ();

	/* Tidy up. */
	funcs->destroy (collection);

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        ((end_time - start_time) * 1000) /
	                        data->n_iterations);
}

static gchar *
test_insert_inspecific (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time;

	/* Create a collection. */
	collection = funcs->create (data->n_elements);

	/* Run the test. */
	start_time = g_get_monotonic_time ();

	for (j = 0; j < data->n_iterations; j++) {
		collection = funcs->insert_inspecific (collection);
	}

	end_time = g_get_monotonic_time ();

	/* Tidy up. */
	funcs->destroy (collection);

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        ((end_time - start_time) * 1000) /
	                        data->n_iterations);
}

static gchar *
test_delete_specific (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time;

	if (data->n_elements < data->n_iterations) {
		return NULL;
	}

	/* Create a collection. */
	collection = funcs->create (data->n_elements);

	/* Run the test. */
	start_time = g_get_monotonic_time ();

	for (j = 0; j < data->n_iterations; j++) {
		collection = funcs->delete_specific (collection,
		                                     data->n_elements - j - 1);
	}

	end_time = g_get_monotonic_time ();

	/* Tidy up. */
	funcs->destroy (collection);

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        (end_time - start_time) * 1000 /
	                        data->n_iterations);
}

static gchar *
test_delete_inspecific (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time;

	if (data->n_elements < data->n_iterations) {
		return NULL;
	}

	/* Create a collection. */
	collection = funcs->create (data->n_elements);

	/* Run the test. */
	start_time = g_get_monotonic_time ();

	for (j = 0; j < data->n_iterations; j++) {
		collection = funcs->delete_inspecific (collection);
	}

	end_time = g_get_monotonic_time ();

	/* Tidy up. */
	funcs->destroy (collection);

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        ((end_time - start_time) * 1000) /
	                        data->n_iterations);
}

static gchar *
test_clear (const CollectionFuncs *funcs, const TestData *data)
{
	guint j;
	gpointer collection;
	gint64 start_time, end_time, aggregate_time = 0;

	/* Run the test. */
	for (j = 0; j < data->n_iterations; j++) {
		/* Create a collection. */
		collection = funcs->create (data->n_elements);

		start_time = g_get_monotonic_time ();
		collection = funcs->clear (collection);
		end_time = g_get_monotonic_time ();

		/* Tidy up. */
		funcs->destroy (collection);

		aggregate_time += (end_time - start_time);
	}

	return g_strdup_printf ("%" G_GINT64_FORMAT,
	                        aggregate_time * 1000 / data->n_iterations);
}


int
main (int argc, char *argv[])
{
	guint i, j, k;
	TestData data;
	static const struct {
		const gchar *name;
		CollectionFuncs funcs;
	} collection_types[] = {
		{ "GList", {
			collection_glist_destroy,
			collection_glist_create,
			collection_glist_iterate,
			collection_glist_insert_specific,
			collection_glist_insert_inspecific,
			collection_glist_delete_specific,
			collection_glist_delete_inspecific,
			collection_glist_clear,
		} },
		{ "GSList", {
			collection_gslist_destroy,
			collection_gslist_create,
			collection_gslist_iterate,
			collection_gslist_insert_specific,
			collection_gslist_insert_inspecific,
			collection_gslist_delete_specific,
			collection_gslist_delete_inspecific,
			collection_gslist_clear,
		} },
		{ "GPtrArray", {
			collection_gptrarray_destroy,
			collection_gptrarray_create,
			collection_gptrarray_iterate,
			collection_gptrarray_insert_specific,
			collection_gptrarray_insert_inspecific,
			collection_gptrarray_delete_specific,
			collection_gptrarray_delete_inspecific,
			collection_gptrarray_clear,
		} },
		{ "GArray", {
			collection_garray_destroy,
			collection_garray_create,
			collection_garray_iterate,
			collection_garray_insert_specific,
			collection_garray_insert_inspecific,
			collection_garray_delete_specific,
			collection_garray_delete_inspecific,
			collection_garray_clear,
		} },
	};

	static const struct {
		const gchar *test_name;
		gchar *(*func) (const CollectionFuncs *funcs,
		                const TestData *data);
	} tests[] = {
		{ "create", test_create },
		{ "iterate", test_iterate },
		{ "insert-specific", test_insert_specific },
		{ "insert-inspecific", test_insert_inspecific },
		{ "delete-specific", test_delete_specific },
		{ "delete-inspecific", test_delete_inspecific },
		{ "clear", test_clear },
	};

	static const guint n_elements[] = {
		0,
		1,
		5,
		10,
		20,
		50,
		100,
		500,
		1000,
		5000,
		10000,
		100000,
		1000000,
	};

	/* Argument parsing. */
	if (argc < 2) {
		g_printerr ("Usage: %s TEST-NAME [N-ITERATIONS]\n", argv[0]);
		return 1;
	}

	if (argc >= 3) {
		data.n_iterations = g_ascii_strtoull (argv[2], NULL, 10);
	} else {
		data.n_iterations = 10000;
	}

	/* Find the specified test. */
	for (j = 0; j < G_N_ELEMENTS (tests); j++) {
		if (strcmp (tests[j].test_name, argv[1]) == 0) {
			break;
		}
	}

	if (j >= G_N_ELEMENTS (tests)) {
		g_printerr ("Invalid test ‘%s’.", argv[1]);
		return 2;
	}

	for (k = 0; k < G_N_ELEMENTS (n_elements); k++) {
		data.n_elements = n_elements[k];

		for (i = 0; i < G_N_ELEMENTS (collection_types); i++) {
			gchar *result;

			result = tests[j].func (&collection_types[i].funcs,
			                        &data);
			if (result == NULL) {
				/* Inapplicable test. */
				continue;
			}

			g_print ("%s, %s, %u, %u, %s\n", tests[j].test_name,
			         collection_types[i].name, data.n_iterations,
			         data.n_elements, result);
			g_free (result);
		}
	}

	return 0;
}
